//
//  CountryCurrencyTableViewCell.h
//  CountryPickerDemo
//
//  Created by Vladimir Bilichenko on 16.03.16.
//  Copyright © 2016 Dmitry Shmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCurrencyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *countryFlag;
@property (weak, nonatomic) IBOutlet UILabel *country;
@property (weak, nonatomic) IBOutlet UILabel *countryCurrency;
@property (weak, nonatomic) IBOutlet UILabel *currencySymbol;

@end
