//  Created by Dmitry Shmidt on 5/11/13.
//  Copyright (c) 2013 Shmidt Lab. All rights reserved.
//  mail@shmidtlab.com

#import <UIKit/UIKit.h>

@interface SLCountryPickerViewController : UIViewController<UISearchBarDelegate>

@property (nonatomic, copy) void (^completionBlock)(NSString *country, NSString *code);
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (void)countryInfoWith:(NSString *)name CountryCode:(NSString *)code CurrencyCode:(NSString *)currencyCode CurrencySymbol:(NSString *)currencySymbol;

- (void)useAllCountries:(BOOL)value;

@end
