//  Created by Dmitry Shmidt on 5/11/13.
//  Copyright (c) 2013 Shmidt Lab. All rights reserved.
//  mail@shmidtlab.com

#import "SLCountryPickerViewController.h"
#import "CountryCurrencyTableViewCell.h"
#import "Constants.h"

static NSString *CellIdentifier = @"CountryCell";
@interface SLCountryPickerViewController ()<UISearchDisplayDelegate, UISearchBarDelegate>
@property (nonatomic) UISearchDisplayController *searchController;
@property (nonatomic, strong, nonnull) NSMutableArray *countriesFromFile;
@property (nonatomic) BOOL useAllCountries;
@end

@implementation SLCountryPickerViewController{
    NSMutableArray *_filteredList;
    NSArray *_sections;
}
//TODO: fix search
- (void)createSearchBar
{
    if(self.searchBar != nil && self.tableView && !self.tableView.tableHeaderView)
    {
        self.searchBar.delegate = self;

        self.searchBar.showsCancelButton = YES;
        
        
        self.tableView.tableHeaderView = self.searchBar;
        
        UISearchDisplayController *searchCon = [[UISearchDisplayController alloc]initWithSearchBar:self.searchBar contentsController:self];
        self.searchController = searchCon;
        _searchController.delegate = self;
        _searchController.searchResultsDataSource = self.tableView.dataSource;
        _searchController.searchResultsDelegate = self.tableView.delegate;
        [self.searchBar becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createSearchBar];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSMutableArray *countryCodes = [NSMutableArray new];
    NSMutableArray *countriesUnsorted = [NSMutableArray new];
    
    self.useAllCountries = NO;
    
    if(self.useAllCountries)
    {
        countryCodes = [[NSMutableArray alloc]initWithArray:[NSLocale ISOCountryCodes]];
    }
    else
    {
        [self initializeCountriesFromFile:countryCodes];
    }
  
    _filteredList = [[NSMutableArray alloc] initWithCapacity:countryCodes.count];
    
    if(self.useAllCountries)
    {
        for (NSString *countryCode in countryCodes)
        {
            NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
            NSDictionary *cd = @{@"name": displayNameString, @"code":countryCode};
            [countriesUnsorted addObject:cd];
            
        }
    }
    else
    {
        for(NSDictionary *country in self.countriesFromFile)
        {
            NSDictionary *cd = @{@"name": [country objectForKey:COUNTRY_NAME], @"code":[country objectForKey:COUNTRY_CODE], @"currencySymbol":[country objectForKey:CURRENCY_SYMBOL]};
            [countriesUnsorted addObject:cd];
            
        }
    }
    
    _sections = [self partitionObjects:countriesUnsorted collationStringSelector:@selector(self)];
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(preferredContentSizeChanged:)
     name:UIContentSizeCategoryDidChangeNotification
     object:nil];

    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
}

- (void)useAllCountries:(BOOL)value
{
    self.useAllCountries = value;
}

- (void)preferredContentSizeChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void) initializeCountriesFromFile:(NSMutableArray *)countries
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Countries" ofType:@"plist"];
    self.countriesFromFile =[NSMutableArray arrayWithContentsOfFile:filePath];
    
    for(NSDictionary *country in self.countriesFromFile)
    {
        [countries addObject:[country objectForKey:COUNTRY_CODE]];
    }
}

#pragma mark - Table view data source
-(NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector
{
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    NSInteger sectionCount = [[collation sectionTitles] count];
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    for (int i = 0; i < sectionCount; i++) {
        [unsortedSections addObject:[NSMutableArray array]];
    }
    
    for (id object in array) {
        NSInteger index = [collation sectionForObject:object[@"name"] collationStringSelector:selector];
        [[unsortedSections objectAtIndex:index] addObject:object];
    }
    
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    
    for (NSMutableArray *section in unsortedSections) {
        NSArray *sortedArray = [section sortedArrayUsingDescriptors:sortDescriptors];
        [sections addObject:sortedArray];
    }
    
    return sections;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [UILocalizedIndexedCollation.currentCollation sectionIndexTitles];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return 1;
    }
    //we use sectionTitles and not sections
    return [[UILocalizedIndexedCollation.currentCollation sectionTitles] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return [_filteredList count];
    }
    return [_sections[section] count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return nil;
    }
    BOOL showSection = [[_sections objectAtIndex:section] count] != 0;
    //only show the section title if there are rows in the section
    return (showSection) ? [[UILocalizedIndexedCollation.currentCollation sectionTitles] objectAtIndex:section] : nil;
    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryCurrencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell)
    {
        cell = [[CountryCurrencyTableViewCell alloc] init];
    }

    NSDictionary *cd = nil;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        cd = _filteredList[indexPath.row];
        
        NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:cd[@"name"] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:0.785 alpha:1.000], NSFontAttributeName:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]}];
        [attributedTitle addAttribute:NSForegroundColorAttributeName
                                value:[UIColor blackColor]
                                range:[attributedTitle.string.lowercaseString rangeOfString:_searchController.searchBar.text.lowercaseString]];
        
        cell.textLabel.attributedText = attributedTitle;
    }
	else
	{
        cd = _sections[indexPath.section][indexPath.row];
        NSLocale *locale = [self countryLocale:cd[@"code"]];
        cell.country.text = cd[@"name"];
        
        cell.countryCurrency.text = [NSString stringWithFormat:@"(%@)",[self currencyCodeByLocale:locale]];
        cell.currencySymbol.text = cd[@"currencySymbol"];
    }
    cell.countryFlag.image = [UIImage imageNamed:cd[@"code"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    Select a country and return to the previous screen.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *cd = nil;
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        cd = _filteredList[indexPath.row];
    }
    else
    {
        cd = _sections[indexPath.section][indexPath.row];
    }
    NSLocale *locale = [self countryLocale:cd[@"code"]];
    [self countryInfoWith:cd[@"name"] CountryCode:cd[@"code"] CurrencyCode:[self currencyCodeByLocale:locale] CurrencySymbol:cd[@"currencySymbol"]];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark Content Filtering

- (void)countryInfoWith:(NSString *)name CountryCode:(NSString *)code CurrencyCode:(NSString *)currencyCode CurrencySymbol:(NSString *)currencySymbol
{
    //Override in child.
}

- (NSLocale *)countryLocale:(NSString *)code
{
    //TODO: check locale and currency symbol...
    NSString *countryCode = code;
    NSDictionary *components = [NSDictionary dictionaryWithObject:countryCode forKey:NSLocaleCountryCode];
    NSString *localeIdent = [NSLocale localeIdentifierFromComponents:components];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:localeIdent];
    return locale;
}

- (NSString *)currencySymbolByLocale:(NSLocale *)locale
{
    return [locale objectForKey:NSLocaleCurrencySymbol];
}

- (NSString *)currencyCodeByLocale:(NSLocale *)locale
{
    return [locale objectForKey: NSLocaleCurrencyCode];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[_filteredList removeAllObjects];
    
    for (NSArray *section in _sections) {
        for (NSDictionary *dict in section)
        {
                NSComparisonResult result = [dict[@"name"] compare:searchText
                                                           options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
                                                             range:NSMakeRange(0, [searchText length])];
                if (result == NSOrderedSame)
                {
                    [_filteredList addObject:dict];
                }
        }
    }
}
#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:
     [self.searchDisplayController.searchBar scopeButtonTitles][[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [self.searchDisplayController.searchBar scopeButtonTitles][searchOption]];
    
    return YES;
}
#pragma mark - searchBar delegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{

}

@end
