Pod::Spec.new do |s|

  s.name         = "CountryPicker"
  s.version      = "0.1"
  s.summary      = "Country currency picker table view controller for iOS 7+"
  s.homepage     = "https://bitbucket.org/byelik/countrycurrencypicker/overview"
  s.license      = 'MIT'
  s.author       = { "Dmitry Shmidt and Vladimir Bilichenko" => "mail@shmidtlab.com" }
  s.source       = { :git => "https://bitbucket.org/byelik/countrycurrencypicker/", :tag => "0.1" }
  s.platform     = :ios, '7.0'
  s.requires_arc = true
  s.screenshots  = "https://bitbucket.org/shmidt/slcountrypicker/raw/bcf4782680ae9595c952726dd086b39c02970bd3/1.jpg", "https://bitbucket.org/shmidt/slcountrypicker/raw/7aab1cc8eca15c9e831c0bf4d998660ab5dbef07/2.jpg"
  s.source_files  = 'CountryPicker', 'CountryPicker/**/*.{h,m}'
  s.resource  = "CountryPicker/CountriesFlags36px.xcassets", "CountryPicker/Countries.plist"
  s.preserve_paths = "CountryPicker/CountriesFlags36px.xcassets/*"
end
