//
//  CountryCurrencyViewController.m
//  CountryPickerDemo
//
//  Created by Vladimir Bilichenko on 15.03.16.
//  Copyright © 2016 Dmitry Shmidt. All rights reserved.
//

#import "CountryCurrencyViewController.h"

@interface CountryCurrencyViewController ()

@end

@implementation CountryCurrencyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//- (void)countryInfo:(NSString *)name CountryCode:(NSString *)code CurrencyCode:(NSString *)currencyCode
- (void)countryInfoWith:(NSString *)name CountryCode:(NSString *)code CurrencyCode:(NSString *)currencyCode CurrencySymbol:(NSString *)currencySymbol
{
    [super countryInfoWith:name CountryCode:code CurrencyCode:currencyCode CurrencySymbol:currencySymbol];
    NSLog(@"%@, %@, %@, %@", name, code, currencyCode, currencySymbol);
}

@end
