//
//  CountryCurrencyViewController.h
//  CountryPickerDemo
//
//  Created by Vladimir Bilichenko on 15.03.16.
//  Copyright © 2016 Dmitry Shmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLCountryPickerViewController.h"

@interface CountryCurrencyViewController : SLCountryPickerViewController

@end
