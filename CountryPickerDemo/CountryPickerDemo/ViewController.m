//
//  ViewController.m
//  CountryPickerDemo
//
//  Created by Dmitry Shmidt on 26/11/13.
//  Copyright (c) 2013 Dmitry Shmidt. All rights reserved.
//

#import "ViewController.h"
#import "SLCountryPickerViewController.h"
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


@end
